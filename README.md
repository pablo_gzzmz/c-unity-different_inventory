# README #
#
Author: Pablo González Martínez
#
pablogonzmartinez@gmail.com

Unity3D Engine
C#

This project showcases the implementation of some UI components used for a simple inventory, as well as tooltip functionality.

# Google Docs doc #
https://docs.google.com/document/d/1uhYT7aHWZHOugQUw-Cb0qYHZ_6Z0NMaKBmLGkCgPle8/edit?usp=sharing

# Requirements #
The project has been created with Unity 2017.1.1f1 (64-bit).

# After-download guide: #
It contains no .exe, as the execution of the project itself ignoring the code is not very relevant. It should be opened in Unity using the parent folder containing ‘Assets’ and ‘Project Settings’ folders.

The project is organized in folders. The main scene is labelled as ‘test’, and is contained in the Scenes folder.

All the code is contained in the Scripts folder and has been created exclusively by the author.