
using UnityEngine;

/// <summary>
/// Class to store generic assets that might want to be referenced across the entire codebase.
/// </summary>

public class GenericAssets : Singleton<GenericAssets> {

    [SerializeField]
    private Sprite emptyIcon = null;
    public  static Sprite EmptyIcon { get { return instance.emptyIcon; } }

}
