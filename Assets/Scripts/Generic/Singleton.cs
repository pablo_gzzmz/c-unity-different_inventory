
using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour {

    protected static T instance;
    public    static T Instance { get { return instance; } }

    private void Awake() {
        if (instance != null) {
            Destroy(gameObject);
            return;
        }

        instance = this as T;
        SingletonAwake();
    }
    
    private void OnDestroy() {
        if (this == instance) instance = null;
        
        SingletonOnDestroy();
    }

    protected virtual void SingletonAwake    () {}
    protected virtual void SingletonOnDestroy() {}
}
