
using UnityEngine;

public class Bounds {

    private Vector2 start;
    public  Vector2 Start { get { return start; } }
    private Vector2 end;
    public  Vector2 End   { get { return end; } }

    private Vector2 size;
    public  Vector2 Size { get { return size; } }

    public Bounds(Vector2 _start, Vector2 _end) {
        start = _start;
        end   = _end;

        size = end - start;
    }

    public bool IsInBounds(Vector2 v) {
        if (v.x < start.x || v.x > end.x) return false;
        if (v.y < start.y || v.y > end.y) return false;
        return true;
    }
}
