
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Globals {

    public const int inventoryMaxSlots = 30;

    public static readonly Color interfaceGray     = new Color(0.7f,  0.65f, 0.65f);
    public static readonly Color interfaceBlack    = new Color(0.15f, 0.15f, 0.15f);
    public static readonly Color interfaceOrangish = new Color(0.95f, 0.75f, 0.4f);

    public static readonly Vector3 offsightUI = new Vector3(-99999, -99999, 0);

    public static readonly Bounds screenBounds = new Bounds(new Vector2(0, 12), new Vector2(0, 0)); // End bounds are subtracted to screen size
}
