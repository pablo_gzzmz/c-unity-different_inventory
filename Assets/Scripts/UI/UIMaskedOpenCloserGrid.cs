
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// A Masked Open Closer that establishes its height results based on a grid of buttons.
/// </summary>

public class UIMaskedOpenCloserGrid : UIMaskedOpenCloser {
    
    [SerializeField]
    private GridLayoutGroup buttonGroup; // Parent grouper of button slots
    public  GridLayoutGroup ButtonGroup { get { return buttonGroup; } }

    // Slots should be controlled from outside classes that will manage this component
    private Button[] slots;
    public  Button[] Slots { get { return slots; } }

    protected override void Start() {
        // The mask should always be part of the grid layout group for the buttons
        maskTransform = buttonGroup.GetComponent<RectTransform>();

        Debug.Assert(maskTransform.GetComponent<RectMask2D>() != null);

        base.Start();
    }

    public void CreateSlots(int n) { slots = new Button[n]; }

    public void GridBasedOpenClose(int count) {
        OpenClose(GetHeight(count));
    }

    public void GridBasedForceOpen(int count) {
        isOpen = false;
        ForceOpen(GetHeight(count));
    }

    private float GetHeight(int count) {
        float heightResult;
        if (!isOpen) {
            heightResult  = startPosition.y;
            heightResult += buttonGroup.cellSize.y + buttonGroup.spacing.y;
            heightResult *= (count+1) / buttonGroup.constraintCount;
            heightResult += buttonGroup.padding.bottom;
            heightResult -= 1; // If icon has outline
        }
        else 
            heightResult = startPosition.y;

        return heightResult;
    }
}
