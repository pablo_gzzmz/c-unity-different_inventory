
using UnityEngine;

/// <summary>
/// Class to depict a tooltip, which contains fragments of text.
/// </summary>

public class Tooltip {
    public static readonly Color tooltipDescriptionDefault = new Color(1f, 0.82f, 0.4f);

    public struct Fragment {
        public string    text;
        public Color     color;
        public int       size;
        public FontStyle style;
    }

    public Fragment   title;
    public Fragment[] description;

    public Tooltip(int descriptionCount, string _title, Color _titleColor) {
        // Default title values
        title.text  = _title;
        title.color = _titleColor;
        title.size  = 22;
        title.style = FontStyle.Normal;
        description = new Fragment[descriptionCount];

        // Default fragment values
        for (int i = 0; i < descriptionCount; i++) {
            description[i].text = "";
            description[i].color = tooltipDescriptionDefault;
            description[i].size = 18;
            description[i].style = FontStyle.Normal;
        }
    }
}