
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Singleton class to manage UI messages shown to the player as an information mechanism.
/// </summary>

public class UIMessageLog : Singleton<UIMessageLog> {

    private const float lifeTime = 5;
    private const int   textAmount = 6;
    private const float spacing = 6;
    
    private static Text [] texts  = new Text [textAmount];
    private static float[] timers = new float[textAmount];

    private static Color alphaSub; // Cached color used to fade

    protected override void SingletonAwake() {
        GameObject cloner = GetComponentInChildren<Text>().gameObject;
        texts[0] = Instantiate(cloner, transform).GetComponent<Text>();
        texts[0].text = "";
        
        for (int i = 1; i < textAmount; i++) {
            texts[i] = Instantiate(cloner, texts[i-1].transform).GetComponent<Text>();
            texts[i].rectTransform.anchorMin = new Vector2(0.5f, 0);
            texts[i].rectTransform.anchorMax = new Vector2(0.5f, 0);
            texts[i].rectTransform.anchoredPosition = new Vector2(0, -spacing);
            texts[i].text = "";
        }

        Destroy(cloner);
    }

    private void Update() {
        for (int i = 0; i < timers.Length; i++) {
            if (timers[i] > 0) {
                timers[i] -= Time.unscaledDeltaTime;

                // Smoothly hide messages as they are about to 'die'
                if (timers[i] <= 1) {
                    alphaSub = texts[i].color;
                    alphaSub.a = timers[i];
                    texts[i].color = alphaSub;
                }
            }
        }
    }

    public static void ReceiveMessage(string message) {
        ReceiveMessage(message, Color.red);
    }

    public static void ReceiveMessage(string message, Color col) {
        for (int i = texts.Length-1; i > 0; i--) {
            texts[i].text = texts[i - 1].text;
            texts[i].color = texts[i - 1].color;
            timers[i] = timers[i - 1];
        }

        texts [0].text = message;
        texts [0].color = col;
        timers[0] = lifeTime;
    }
}
