
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Singleton controller for the display of tooltips as UI images.
/// </summary>

public class TooltipController : Singleton<TooltipController> {

    private static RectTransform rect; // Transform to move
    private static RectTransform root; // Transform to change size
    private static Text title;
    private static List<Text> description = new List<Text>();

    private static bool display = false;
    private static bool update  = false;
    private static bool hidden  = false;

    private const float margin  = 6;
    private const float padding = 14;
    
    private static readonly Vector3 mouseOffset = new Vector2(24, -32f);

    private static WaitForEndOfFrame wait;

    private void Start() {
        root = transform.Find("Root").GetComponent<RectTransform>();
        rect = GetComponent<RectTransform>();
        title = root.transform.Find("Title").GetComponent<Text>();
        description.Add(title.transform.Find("Text").GetComponent<Text>());
        
        wait = new WaitForEndOfFrame();

        ClearTooltip();
        transform.SetAsLastSibling();
    }

    private void Update() {
        if (hidden) {
            rect.anchoredPosition = Globals.offsightUI;
        }

        if (!update) return;
        
        Vector2 pos = Input.mousePosition + mouseOffset;
        MaintainBounds(ref pos);
        
        pos.x = Mathf.Round(pos.x);
        pos.y = Mathf.Round(pos.y);
        rect.anchoredPosition = pos;
    }

    private void MaintainBounds(ref Vector2 pos) {
        Vector2 size = root.sizeDelta;

        // Y
        if (pos.y - size.y < Globals.screenBounds.Start.y)
            pos.y = size.y + Globals.screenBounds.Start.y;
        else 
        if (pos.y > Screen.height - Globals.screenBounds.End.y)
            pos.y = Screen.height - Globals.screenBounds.End.y;
        
        // X
        if (pos.x < Globals.screenBounds.Start.x)
            pos.x = Globals.screenBounds.Start.x;
        else 
        if (pos.x + size.x > Screen.width - Globals.screenBounds.End.x)
            pos.x = Screen.width - size.x - Globals.screenBounds.End.x;
    }

    public static void DisplayTooltip(Tooltip tooltip) {
        PassTooltip(tooltip.title, title);

        // Automatically add new texts if needed
        if (tooltip.description.Length >= description.Count) {
            for (int i = 0; i < description.Count; i++) {
                PassTooltip(tooltip.description[i], description[i]);
            }

            for (int i = description.Count; i < tooltip.description.Length; i++) {
                description.Add(Instantiate(description[0], title.transform));
                PassTooltip(tooltip.description[i], description[i]);
            }
        }
        else {
            for (int i = 0; i < tooltip.description.Length; i++) {
                PassTooltip(tooltip.description[i], description[i]);
            }

            for (int i = tooltip.description.Length; i < description.Count; i++) {
                description[i].text = "";
            }
        }
        

        instance.StartCoroutine(SetSizeAndPos(tooltip.description.Length));
	}
    
    public static void ClearTooltip() {
        instance.StartCoroutine(Clear());
    }

    private static void Hide() { hidden = true;  }
    private static void Show() { hidden = false; } 

    private static void PassTooltip(Tooltip.Fragment from, Text to) {
        to.text      = from.text;
        to.color     = from.color;
        to.fontSize  = from.size;
        to.fontStyle = from.style;
    }
    
    private static IEnumerator Clear() {
        display = false;

        yield return null;

        if (!display) {
            update = false;

            rect.anchoredPosition = Globals.offsightUI;
        }
    }

    private static IEnumerator SetSizeAndPos(int textAmount) {
        display = true;

        // A wait is necessary so that we can properly identify the new sizes thanks to 'Content Size Fitter' components
        yield return wait;
        
        // Ensure we haven't cleared the tooltip while waiting
        if (display) {
            update = true;

            Vector2 totalImageSize = Vector2.zero;
            // Title size
            totalImageSize.y = title.rectTransform.sizeDelta.y;
            totalImageSize.x = title.rectTransform.sizeDelta.x + margin;

            if (textAmount > 0) {
                // Starting description position
                Vector2 v = description[0].rectTransform.anchoredPosition;

                // For every description text
                for (int i = 0; i < textAmount; i++) {
                    // Current size plus margin
                    float currentY = description[i].rectTransform.sizeDelta.y + margin;
                    
                    // Add to total size
                    totalImageSize.y += currentY;

                    // Check for biggest X size
                    float currentX = description[i].rectTransform.sizeDelta.x + margin;
                    if (totalImageSize.x < currentX) totalImageSize.x = currentX;

                    // First description does not require positioning
                    if (i == 0) continue;

                    // Set position
                    v.y -= currentY; 
                    description[i].rectTransform.anchoredPosition = v;
                }
            }
            
            // Add padding
            totalImageSize.x += padding;
            totalImageSize.y += padding;
            
            root.sizeDelta = totalImageSize;
        }
    }
}
