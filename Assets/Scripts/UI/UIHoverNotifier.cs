
using UnityEngine;
using UnityEngine.EventSystems;
using System;

public class UIHoverNotifier : MonoBehaviour, ISelectHandler, IPointerEnterHandler, IPointerExitHandler {

    public event Action OnSelection;
    public event Action<bool> OnHover;
    
    public virtual void OnSelect(BaseEventData evd) {
        if (OnSelection != null) OnSelection.Invoke();
    }

    public void OnPointerEnter(PointerEventData eventData) {
        if (OnHover != null) OnHover.Invoke(true);
    }

    public void OnPointerExit(PointerEventData eventData) {
        if (OnHover != null) OnHover.Invoke(false);
    }
}
