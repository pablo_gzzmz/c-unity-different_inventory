
using UnityEngine;

/// <summary>
/// This class ensures pixel perfect display based on screen size's pixels being even or not even.
/// </summary>

[RequireComponent(typeof(RectTransform))]
public class UIPixelPerfectPositionFixer : MonoBehaviour {
    
    private RectTransform rectTransform;

    private Vector2 originalPosition;

    [SerializeField]
    private bool fixX = true;
    [SerializeField]
    private bool fixY = true;

    private void Start () {
        rectTransform = GetComponent<RectTransform>();

        originalPosition = rectTransform.anchoredPosition;
        originalPosition.x = Mathf.Round(originalPosition.x);
        originalPosition.y = Mathf.Round(originalPosition.y);
        FixPosition();

        ScreenController.OnScreenSizeChange += FixPosition;
	}

    private void OnDestroy() {
        ScreenController.OnScreenSizeChange -= FixPosition;
    }

    private void FixPosition() {
        Vector2 fixedPos = rectTransform.anchoredPosition;

        if (fixX) {
            if ((Screen.width & 1) == 0)
                fixedPos.x = originalPosition.x;
            else
                fixedPos.x = originalPosition.x + 0.5f;
        }

        if (fixY) {
            if ((Screen.height & 1) == 0)
                fixedPos.y = originalPosition.y;
            else
                fixedPos.y = originalPosition.y - 0.5f;
        }
        
        rectTransform.anchoredPosition = fixedPos;
    }
}
