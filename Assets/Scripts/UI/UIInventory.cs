
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Text;

/// <summary>
/// Class which controls the UI display of the local player's inventory.
/// </summary>

public class UIInventory : MonoBehaviour {

    [SerializeField]
    private GridLayoutGroup grouper = null;

    private Inventory inventory;
    
    private struct Category {
        public Text label;

        public UIMaskedOpenCloserGrid openCloser;

        public Image inventoryFill; // Fill bar to signal the amount of space taken by this category
        public Image totalFill;     // Fill bar to signal the total amount of space taken
        
        public string tooltipSubtitle;
    }
    
    private Category[] categories = new Category[(int)ItemCategory.Count];

    private const float categoryOpenCloseTime = 0.1f; // Duration of the damp 'animation'

    // Tooltip
    private Tooltip infoTooltip;
    private StringBuilder builder;
    private int builderEraseIndex;   // To reuse stringBuilder without requiring a new one
    private int showingTooltip = -1; // As an index of the current category being shown

    private void Start(){
        InitializeTooltip();

        InitializeCategories();

        inventory.OnInventoryChange += SetInfo;
    }

    public void SetInventory(Inventory _inventory){
        inventory = _inventory;
    }

    private void Destroy() {
        inventory.OnInventoryChange -= SetInfo;
    }

    private void InitializeTooltip() {
        infoTooltip = new Tooltip(2, "Inventory", Color.white);
        infoTooltip.description[0].color = Globals.interfaceOrangish;
        infoTooltip.description[1].color = Globals.interfaceGray;
        builder = new StringBuilder("Space:  ", 4);

        builderEraseIndex = builder.Length; // Save erase index to facilitate reusing the same string builder
    }
    
    private void InitializeCategories() {
        GameObject categoryPrefab = Resources.Load<GameObject>("UI/InventoryCategory");
        GameObject buttonPrefab   = Resources.Load<GameObject>("UI/DefaultButton");
        GameObject go;

        // Category creation based on enum
        for (int i = 0; i < categories.Length; i++) {
            ItemCategory category = (ItemCategory)i;
            int index = i; // Necessary to store for delegates

            go = Instantiate(categoryPrefab, grouper.transform);

            // Main object
            Transform main = go.transform.Find("Main");
            categories[i].label = main.Find("Label").GetComponent<Text>();

            //Setup notifier to showcase a tooltip when hovering
            UIHoverNotifier notifierMain = main.gameObject.AddComponent<UIHoverNotifier>();
            notifierMain.OnHover += (bool show) => { ShowTooltip(show, index); };
        
            string label = category.ToString();
            categories[i].label.text = label.ToUpper();
            categories[i].tooltipSubtitle = label + " items.";

            // Setup the main button to open/close the category
            main.gameObject.GetComponent<Button>().onClick.AddListener(() => {
                int count = inventory.ItemsCount(category);
                if (count > 0) {
                    categories[index].openCloser.GridBasedOpenClose(count);
                }
                else {
                    UIMessageLog.ReceiveMessage("Tried to open an empty inventory category.");
                }
            });
            
            // Fillers setup
            categories[i].inventoryFill = go.transform.Find("Fill"     ).GetComponent<Image>();
            categories[i].totalFill     = go.transform.Find("TotalFill").GetComponent<Image>();

            //Set OpenCloser reference
            categories[i].openCloser = go.GetComponent<UIMaskedOpenCloserGrid>();

            // Buton creation
            categories[i].openCloser.CreateSlots(Globals.inventoryMaxSlots);

            for (int n = 0; n < categories[i].openCloser.Slots.Length; n++) {
                go = Instantiate(buttonPrefab, categories[i].openCloser.ButtonGroup.transform);
                Button slot = go.GetComponent<Button>();
                categories[i].openCloser.Slots[n] = slot;

                int number = n;
                slot.onClick.AddListener(() => { UseSlot(category, number); });

                // Setup button tooltip show for items
                UIHoverNotifier notifierButton = slot.gameObject.AddComponent<UIHoverNotifier>();
                notifierButton.OnHover += (bool show) => {
                    ItemData data = inventory.GetItemData(category, number);
                    if(data != null)
                        data.ShowTooltip(show);
                };
            }
        }

        for (int i = 0; i < categories.Length; i++) SetInfo ((ItemCategory)i);
    }

    private void UseSlot(ItemCategory category, int slotIndex) {
        ItemData data = inventory.GetItemData(category, slotIndex);

        if (data != null) {
            UIMessageLog.ReceiveMessage("Clicked on: "+data.name, Color.white);
        }
    }

    private void SetInfo(ItemCategory category) {
        int index = (int)category;
        int localItemCount = inventory.ItemsCount(category);

        // Set fill of this category
        categories[index].inventoryFill.fillAmount = (float)localItemCount / Globals.inventoryMaxSlots;

        // Set total fill of all categories
        for (int i = 0; i < categories.Length; i++) {
            categories[i].totalFill.fillAmount = (float)inventory.Count / Globals.inventoryMaxSlots;
        }

        // Set buttons
        for (int i = 0; i < localItemCount; i++) {
            categories[index].openCloser.Slots[i].image.sprite = inventory.GetItemData(category, i).Icon;
        }
        for (int i = localItemCount; i < Globals.inventoryMaxSlots; i++) {
            categories[index].openCloser.Slots[i].image.sprite = GenericAssets.EmptyIcon;
        }

        SetTooltip();
        if (showingTooltip == index) ShowTooltip(true, index); // Refresh tooltip if necessary

        // Re-open if necessary
        if (categories[index].openCloser.IsOpen) {
            int count = inventory.ItemsCount(category);
            if (count > 0) {
                categories[index].openCloser.GridBasedForceOpen(inventory.ItemsCount(category));
            }
            else {
                categories[index].openCloser.ForceClose();
            }
        }
    }
    
    private void ShowTooltip(bool show, int index) {
        infoTooltip.description[0].text = builder.ToString();
        infoTooltip.description[1].text = categories[index].tooltipSubtitle;

        if (show) {
            TooltipController.DisplayTooltip(infoTooltip);
            showingTooltip = index;
        }
        else {
            TooltipController.ClearTooltip();
            showingTooltip = -1;
        }
    }

    private void SetTooltip() {
        builder.Remove(builderEraseIndex, builder.Length-builderEraseIndex);
        builder.Append(inventory.Count);
        builder.Append(" / ");
        builder.Append(Globals.inventoryMaxSlots);
    }
}
