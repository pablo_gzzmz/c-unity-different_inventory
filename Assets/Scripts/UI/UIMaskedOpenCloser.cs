
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Component used to allow a masked rect containing buttons to be opened and closed smoothly via damp function.
/// </summary>

[Serializable]
public class UIMaskedOpenCloser : MonoBehaviour {

    private const float openCloseTime = 0.1f;

    [SerializeField]
    protected RectTransform  rectTransform; // Element to move up and down
    protected Vector2        startPosition;

    [SerializeField]
    protected RectTransform  maskTransform; // Transform to control the mask for the button slots

    protected bool isOpen;
    public    bool IsOpen { get { return isOpen; } }
    protected bool moving;

    private IEnumerator coroutine; // Open-close 'animation' coroutine

    protected virtual void Start() {
        startPosition = rectTransform.anchoredPosition;

        coroutine = GoToHeightCoroutine(0); // To avoid potential null exception on first use
    }

    /// <summary>
    /// Opens or closes the panel given the button count, considering button height and button group constraints.
    /// </summary>
    public void OpenClose(float height) {
        isOpen = !isOpen;
        
        if (moving) StopCoroutine(coroutine);

        coroutine = GoToHeightCoroutine(height);
        StartCoroutine(coroutine);
    }
    
    public void ForceOpen(float height) {
        isOpen = false;
        OpenClose(height);
    }

    public void ForceClose() {
        isOpen = true;
        OpenClose(0);
    }

    private IEnumerator GoToHeightCoroutine(float heightResult) {
        moving = true;

        float velocityPos  = 0;
        float velocitySize = 0;
        Vector2 pos  = rectTransform.anchoredPosition;
        Vector2 size = maskTransform.sizeDelta;
        
        while (Mathf.Abs(heightResult - pos.y) > 1f) {
            // Move
            pos.y = Mathf.SmoothDamp(pos.y, heightResult, ref velocityPos, openCloseTime, 5000, Time.unscaledDeltaTime);
            rectTransform.anchoredPosition = pos;

            // Resize mask
            size.y = Mathf.SmoothDamp(size.y, heightResult, ref velocitySize, openCloseTime, 5000, Time.unscaledDeltaTime);
            maskTransform.sizeDelta = size;
            yield return null;
        }

        pos.y  = heightResult;
        size.y = heightResult;
        rectTransform.anchoredPosition = pos;
        maskTransform.sizeDelta    = size;
        
        moving = false;
    }
}
