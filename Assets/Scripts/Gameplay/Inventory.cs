
using UnityEngine;
using System.Collections.Generic;
using System;

/// <summary>
/// Class to depict scriptable object items in an inventory.
/// </summary>

public class Inventory {

    // Categories
    private Dictionary<ItemCategory, List<ItemData>> items;

    // The count is the total count of the whole inventory
    private int count = 0;
    public  int Count { get { return count; } }

    public event Action<ItemCategory> OnInventoryChange;
    public event Action<ItemData> OnItemAdded;
    public event Action<ItemData> OnItemRemoved;
    
    public Inventory() {
        // Setup dictionary
        items = new Dictionary<ItemCategory, List<ItemData>> ();

        int amount = (int)ItemCategory.Count;
        for (int i = 0; i < amount; i++) {
            items.Add((ItemCategory)i, new List<ItemData>());
        }
    }

    public int ItemsCount(ItemCategory category) {
        return items[category].Count;
    }

    public ItemData GetItemData(ItemCategory category, int index) {
        List<ItemData> list = items[category];

        if (list.Count > index)
            return list[index];

        return null;
    }

    public void AddItemData(ItemData data) {
        if (count == Globals.inventoryMaxSlots) {
            UIMessageLog.ReceiveMessage("Tried to add an item when the inventory is full.");
            return;
        }
        
        items[data.Category].Add(data);

        count++;
        CallOnInventoryChange(data.Category);
        if (OnItemAdded != null) OnItemAdded.Invoke(data);
    }

    public void RemoveItemData(ItemData data) {
        if (!items[data.Category].Remove(data)) return;

        count--;
        CallOnInventoryChange(data.Category);
        if (OnItemRemoved != null) OnItemRemoved.Invoke(data);
    }

    private void CallOnInventoryChange(ItemCategory category) {
        if (OnInventoryChange != null) OnInventoryChange.Invoke(category);
    }
}
