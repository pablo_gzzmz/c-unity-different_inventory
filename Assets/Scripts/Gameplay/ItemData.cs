
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item", menuName = "Database/Item")]
public class ItemData : ScriptableObject {

    [SerializeField]
    private Sprite icon;
    public  Sprite Icon { get { return icon; } }

    [SerializeField]
    private ItemCategory category;
    public  ItemCategory Category { get { return category; } }

    [Multiline]
    [SerializeField]
    private string description;

    private Tooltip tooltip;

    private void OnValidate() {
        tooltip = new Tooltip(2, name, Globals.interfaceOrangish);

        tooltip.description[0].color = Globals.interfaceGray;
        tooltip.description[0].text  = "Category: "+category.ToString();

        tooltip.description[1].color = Color.white;
        tooltip.description[1].text  = description;
    }

    public void ShowTooltip(bool show) {
        if (show) {
            TooltipController.DisplayTooltip(tooltip);
        }
        else {
            TooltipController.ClearTooltip();
        }
    }
}
