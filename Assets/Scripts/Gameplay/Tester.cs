
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tester : MonoBehaviour {

    [SerializeField]
    private UIInventory uiInventory;

    private Inventory inventory;

    private ItemData[] phItems = new ItemData[(int)ItemCategory.Count];

    private void Awake() {
        inventory = new Inventory();

        uiInventory.SetInventory(inventory);
    }

    private void Start() {
        for (int i = 0; i < phItems.Length; i++) {
            phItems[i] = Resources.Load<ItemData>("Database/Item"+(i+1));
        }
    }
       
	private void Update () {
        if (Input.GetKeyDown(KeyCode.Alpha1)) {
            AddOrRemove(phItems[0]);
        }

        else if (Input.GetKeyDown(KeyCode.Alpha2)) {
            AddOrRemove(phItems[1]);
        }

        else if (Input.GetKeyDown(KeyCode.Alpha3)) {
            AddOrRemove(phItems[2]);
        }
	}

    private void AddOrRemove(ItemData data) {
        bool remove = Input.GetKey(KeyCode.LeftShift);

        if (remove) inventory.RemoveItemData(data);
        else inventory.AddItemData(data);
    }
}
